provider "google" {
    project = "analog-medium-374702"
    region = "asia-southeast2"
}

terraform {
    backend "gcs" {
        bucket = "binar-tf-state-syaifuddin"
        prefix = "project-terraform-sql"
    }
}